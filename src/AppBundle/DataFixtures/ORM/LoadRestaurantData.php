<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Restaurant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\ORMFixtureInterface;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadRestaurantData extends Fixture
{
    const FAIZA = 'Фаиза';
    const DOMINO = 'Домино-пицца';
    public function load(ObjectManager $manager)
    {

        $restaurant1 = new Restaurant();
        $restaurant1->setName('Фаиза')
                   ->setDescription('Популярное кафе "Фаиза" на севере столицы, 
                   предлагающее разнообразное меню по приемлемым ценам. Прекрасное место для проведения ланча и обедов')
                  ->setImageName('faiza.png');

        $manager->persist($restaurant1);

        $restaurant2 = new Restaurant();
        $restaurant2->setName('Домино-пицца')
            ->setDescription('Доставим пиццу за 60 минут или получите ее бесплатно')
            ->setImageName('domino.png');

        $manager->persist($restaurant2);

        $manager->flush();

        $this->addReference(self::FAIZA, $restaurant1);
        $this->addReference(self::DOMINO, $restaurant2);
    }
}
