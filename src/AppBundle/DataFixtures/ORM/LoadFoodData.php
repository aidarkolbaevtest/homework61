<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Food;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFoodData extends Fixture implements DependentFixtureInterface
{
    const FOOD_1 = '1';
    const FOOD_2 = '2';
    const FOOD_3 = '3';
    const FOOD_4 = '4';
    const FOOD_5 = '5';
    const FOOD_6 = '6';
    const FOOD_7 = '7';
    const FOOD_8 = '8';
    const FOOD_9 = '9';
    const FOOD_10 = '10';

    public function load(ObjectManager $manager)
    {
        $pizzas = [
            'Азиатская мясная',
            'Пицца LOVE',
            'Американка',
            'Американка по-восточному',
            'Амигос'
        ];
        for ($i = 0; $i < 5; $i++) {
            $pizza = new Food();
            $pizza->setName($pizzas[$i])
               ->setPrice(rand(400, 2500))
                ->setRestaurant($this->getReference(LoadRestaurantData::DOMINO))
                ->setImageName('food' . ($i+1) . '.png');

            $manager->persist($pizza);
            switch ($i) {
                case 0:
                    $this->addReference(self::FOOD_1, $pizza);
                    break;
                case 1:
                    $this->addReference(self::FOOD_2, $pizza);
                    break;
                case 2:
                    $this->addReference(self::FOOD_3, $pizza);
                    break;
                case 3:
                    $this->addReference(self::FOOD_4, $pizza);
                    break;
                case 4:
                    $this->addReference(self::FOOD_5, $pizza);
                    break;
            }
        }

        $foods = [
            'Котлета в кляре с пюре',
            'Котлета в кляре с гречкой',
            'Котлета в кляре с рисом',
            'Бифштекс с яйцом и пюре',
            'Бефстроганов с пюре'
        ];
        for ($j = 0; $j < 5; $j++) {
            $food = new Food();
            $food->setName($foods[$j])
                ->setPrice(rand(120, 1000))
                ->setRestaurant($this->getReference(LoadRestaurantData::FAIZA))
                ->setImageName('food' . ($j + 6) . '.png');

            $manager->persist($food);

            switch ($j) {
                case 0:
                    $this->addReference(self::FOOD_6, $food);
                    break;
                case 1:
                    $this->addReference(self::FOOD_7, $food);
                    break;
                case 2:
                    $this->addReference(self::FOOD_8, $food);
                    break;
                case 3:
                    $this->addReference(self::FOOD_9, $food);
                    break;
                case 4:
                    $this->addReference(self::FOOD_10, $food);
                    break;
            }
        }
            $manager->flush();

    }


    public function getDependencies()
    {
        return array(
            LoadRestaurantData::class,
        );
    }
}
