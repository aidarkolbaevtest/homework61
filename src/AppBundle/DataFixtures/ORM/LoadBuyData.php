<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Buy;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadBuyData extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {

        for ($i = 0; $i < 5; $i++) {
            for ($j = 0; $j < 7; $j++) {
                $buy = new Buy();
                $buy->setFood($this->getReference(LoadFoodData::FOOD_1))
                ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 4) . '-2017'));

                $manager->persist($buy);
            }

            for ($k = 0; $k < 3; $k++) {
                $buy2 = new Buy();
                $buy2->setFood($this->getReference(LoadFoodData::FOOD_2))
                    ->setDate(new \DateTime('0' . rand(1, 9) . '-0' . ($i + 5) . '-2017'));

                $manager->persist($buy2);
            }

            for ($l = 0; $l < 9; $l++) {
                $buy3 = new Buy();
                $buy3->setFood($this->getReference(LoadFoodData::FOOD_3))
                    ->setDate(new \DateTime('0' . rand(1, 9) . '-0' . ($i + 5) . '-2017'));

                $manager->persist($buy3);
            }

            $buy4 = new Buy();
            $buy4->setFood($this->getReference(LoadFoodData::FOOD_4))
                ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 2) . '-2017'));

            $manager->persist($buy4);

            for ($m = 0; $m < 2; $m++) {
                $buy5 = new Buy();
                $buy5->setFood($this->getReference(LoadFoodData::FOOD_5))
                    ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 4) . '-2017'));

                $manager->persist($buy5);
            }


            for ($n = 0; $n < 13; $n++) {
                $buy6 = new Buy();
                $buy6->setFood($this->getReference(LoadFoodData::FOOD_6))
                    ->setDate(new \DateTime('0' . 1 . '-0' . ($i + 1) . '-2018'));

                $manager->persist($buy6);
            }


            for ($o = 0; $o < 29; $o++) {
                $buy7 = new Buy();
                $buy7->setFood($this->getReference(LoadFoodData::FOOD_7))
                    ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 5) . '-2017'));

                $manager->persist($buy7);
            }

            $buy8 = new Buy();
            $buy8->setFood($this->getReference(LoadFoodData::FOOD_8))
                ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 5) . '-2017'));

            $manager->persist($buy8);

            for ($p = 0; $p < 2; $p++) {
                $buy9 = new Buy();
                $buy9->setFood($this->getReference(LoadFoodData::FOOD_9))
                    ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 2) . '-2017'));

                $manager->persist($buy9);
            }

            for ($q = 0; $q < 3; $q++) {
                $buy10 = new Buy();
                $buy10->setFood($this->getReference(LoadFoodData::FOOD_10))
                    ->setDate(new \DateTime('' . rand(10, 28) . '-0' . ($i + 4) . '-2017'));

                $manager->persist($buy10);
            }
        }

        $manager->flush();
    }


    public function getDependencies()
    {
        return array(
            LoadFoodData::class,
        );
    }
}
