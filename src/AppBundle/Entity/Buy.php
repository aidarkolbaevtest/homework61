<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Buy
 *
 * @ORM\Table(name="buy")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BuyRepository")
 */
class Buy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * @var Food
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Food", inversedBy="buy")
     */
    private $food;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Buy
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param Food $food
     * @return Buy
     */
    public function setFood(Food $food): Buy
    {
        $this->food = $food;
        return $this;
    }

    /**
     * @return Food
     */
    public function getFood(): Food
    {
        return $this->food;
    }
}

