<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $restaurants = $this->getDoctrine()->getRepository('AppBundle:Restaurant')->findAll();
        $places = [];

        foreach ($restaurants as $restaurant) {
            $foods = $this->getDoctrine()
                ->getRepository('AppBundle:Food')
                ->getMostPopularFoods($restaurant, 2, 365);
            $places[] = [
                'restaurant' => $restaurant,
                'foods' => $foods
            ];
        }
        dump($places);
        return $this->render('@App/Restaurants/index.html.twig', [
            'places' => $places
        ]);
    }

    /**
     * @Route("/restaurant/{id}", requirements={"id" = "\d+"}, name="details")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function detailsAction($id)
    {
        $restaurant = $this->getDoctrine()->getRepository('AppBundle:Restaurant')->find($id);
        return $this->render('@App/Restaurants/details.html.twig', [
             'restaurant' => $restaurant,
             'foods' => $restaurant->getFoods()
        ]);
    }
}
